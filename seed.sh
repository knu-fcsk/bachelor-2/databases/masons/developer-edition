#!/usr/bin/env bash

echo "Start: $(date)"

echo "Seeding backend_db.."
psql "postgres://backend:backend@backend_db/backend?sslmode=disable" -f $SEED_FILES_DIR/seed.sql

echo "Finish: $(date)"
exit